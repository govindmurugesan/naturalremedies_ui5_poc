 var oController ="";
sap.ui.controller("chart3D.view.cattle", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf view.cattle
*/
	onInit: function() {
		
},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf view.cattle
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf view.cattle
*/
	onAfterRendering: function() {
		sap.ui.getCore().byId("idcattle--cattleChart").addStyleClass("Show");
		sap.ui.getCore().byId("idcattle--poultryChart").addStyleClass("Hide");
		sap.ui.getCore().byId("idcattle--exportsChart").addStyleClass("Hide");
		sap.ui.getCore().byId("idcattle--combineChart").addStyleClass("Hide");
		
		oController = this;
		jQuery(function() {
			var chart = oController.getView().byId("cattleChart");
			/*	var processed_json = new Array();   
            $.getJSON('charts_demo/data.json', function(data) {
                // Populate series
                for (i = 0; i < data.length; i++){
                    processed_json.push([data[i].key, data[i].value]);
                }
            });*/
			var oModel = new sap.ui.model.json.JSONModel();
			 oModel.loadData("json/cattle.json");
			 chart.setModel(oModel);
			chart = chart.getId();
			$("#" + chart).highcharts({
				chart: {
					type: "column",
					options3d: {
			            enabled: true,
			            alpha: 10,
			            beta: 10,
			            viewDistance: 15,
			            depth: 50
			        },
				    height: null,
					width: null
				},
				xAxis: {
			        categories: ['15-16 ACH', '16-17 TGT', '16-17 ACH', 'ACH%', 'GR%','INCR','SUR/DEF']
			    },
			    yAxis: {
			        allowDecimals: false,
			       /* min: 0,*/
			        title: {
			            text: 'MTD-YTD SALES',
			           /* measures : "{value1,value2,value3,value4,value5,value6,value7}",*/
			            style: { color: "#4DA74D" }
			        }
			    },
				 colors: ["#FBB321", "#4DA74D", "#E7191B", "#EF8424", "#C2C2C2"], 
				title: {
			        text: 'Cattle',
			        style: { color: "#4DA74D" }
			    },
				plotOptions: {
			        column: {
			            stacking: 'normal',
			            depth: 20
			        }
			    },
			    credits: {
			        enabled: false
			    },
				/*legend: {
					enabled: true,
					layout: "horizontal",
					align: "right",
					width: 850,
					verticalAlign: "bottom"
				},*/
				series: [
						{"name": "Dec 2016-17","data": [9.78, 11.45, 7.62, 67, -22, -2.17, -3.83],"stack": "1"},
						 {"name": "Apr Dec 2016-17","data": [80.89, 95.22, 83.57, 88, 3, 2.68, -11.66],"stack": "2"}
						
				        ]
			});
		});
//////////////////// poultryChart ///////////	
		oController = this;
		jQuery(function() {
			var chart = oController.getView().byId("poultryChart");
			chart = chart.getId();
			$("#" + chart).highcharts({
				chart: {
					type: "column",
					options3d: { enabled: true, alpha: 10, beta: 10, viewDistance: 15, depth: 50 }
				},
				xAxis: {
			        categories: ['15-16 ACH', '16-17 TGT', '16-17 ACH', 'ACH%', 'GR%','INCR','SUR/DEF']
			    },
			    yAxis: {
			        allowDecimals: false,
			        title: {
			            text: 'MTD-YTD SALES',
			            style: { color: "#4DA74D" }
			        }
			    },colors: ["#E7191B", "#FBB321", "#EF8424", "#C2C2C2", "#4DA74D"], 
				title: {
			        text: 'Poultry',
			        style: { color: "#4DA74D" }
			    },
				plotOptions: {
			        column: {
			            stacking: 'normal',
			            depth: 20
			        }
			    },
			    credits: { enabled: false },
				series: [
						{"name": "Dec 2016-17","data": [2.69, 3.96, 2.88, 73, 7, 0.20, -1.07]},
						 {"name": "Apr to Dec 2016-17","data": [25.15, 33.23, 26.73, 80, 6, 1.58, -6.50]}
					] });
			});
/////////////////////// exportsChart //////////////////
		oController = this;
		jQuery(function() {
			var chart = oController.getView().byId("exportsChart");			
			chart = chart.getId();
			$("#" + chart).highcharts({
				chart: {
					type: "column",
					options3d: { enabled: true, alpha: 10, beta: 10, viewDistance: 15, depth: 50 }
				},
				xAxis: {
			        categories: ['15-16 ACH', '16-17 TGT', '16-17 ACH', 'ACH%', 'GR%','INCR','SUR/DEF']
			    },
			    yAxis: {
			        allowDecimals: false,
			        title: {
			            text: 'MTD-YTD SALES',
			            style: { color: "#4DA74D" }
			        }
			    },colors: ["#EF8424", "#C2C2C2", "#E7191B", "#FBB321", "#4DA74D"], 
				title: {
			        text: 'Exports',
			        style: { color: "#4DA74D" }
			    },
				plotOptions: {
			        column: {
			            stacking: 'normal',
			            depth: 20
			        }
			    },
			    credits: { enabled: false },
				series: [
						{"name": "Dec 2016-17","data": [1.78, 1.53, 1.70, 111, -4, -0.07, 0.17],"stack": "1"},
						 {"name": "Apr to Dec 2016-17","data": [13.17, 16.05, 12.86, 80, -2, -0.31, -3.19],"stack": "2"}
					] });
			});
////////////////////// combineChart //////////////////
		oController = this;
		jQuery(function() {
			var chart = oController.getView().byId("combineChart");			
			chart = chart.getId();
			$("#" + chart).highcharts({
				chart: {
					type: "column",
					options3d: {
			            enabled: true,
			            alpha: 15,
			            beta: 15,
			            viewDistance: 25,
			            depth: 40
			        }
				},
				xAxis: {
			        categories: ['15-16 ACH', '16-17 TGT', '16-17 ACH', 'ACH%', 'GR%','INCR','SUR/DEF']
			    },
			    yAxis: {
			        allowDecimals: false,
			        title: {
			            text: 'MTD-YTD SALES',
			            style: { color: "#4DA74D" }
			        }
			    },/*colors: ["#EF8424", "#C2C2C2", "#E7191B", "#FBB321", "#4DA74D"], */
				title: {
			        text: 'All Divisions',
			        style: { color: "#4DA74D" }
			    },
				plotOptions: {
			        column: {
			            stacking: 'normal',
			            depth: 20
			        }
			    },
			    credits: { enabled: false },
				series: [
						{"name": "Cattle Dec 2016-17","data": [9.78, 11.45, 7.62, 67, -22, -2.17, -3.83],"stack": "1"},
						{"name": "Cattle Apr Dec 2016-17","data": [80.89, 95.22, 83.57, 88, 3, 2.68, -11.66],"stack": "1"},
						{"name": "Poultry Dec 2016-17","data": [2.69, 3.96, 2.88, 73, 7, 0.20, -1.07], "stack": "2"},
						{"name": "Poultry Apr to Dec 2016-17","data": [25.15, 33.23, 26.73, 80, 6, 1.58, -6.50], "stack": "2"},
						{"name": "Exports Dec 2016-17","data": [1.78, 1.53, 1.70, 111, -4, -0.07, 0.17], "stack": "3"},
						{"name": "Exports Apr to Dec 2016-17","data": [13.17, 16.05, 12.86, 80, -2, -0.31, -3.19], "stack": "3"}
					] });
			});
		
		
	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf view.cattle
*/
//	onExit: function() {
//
//	}
	onBack: function(){
		sap.ui.getCore().byId("reference").to("idsubDashboard");
	  },
	selectOption: function(){
		var Selectedvalue =	this.getView().byId("comboId").getSelectedItem().getText();
		if(Selectedvalue=="Poultry"){
			sap.ui.getCore().byId("idcattle--poultryChart").addStyleClass("Show");
			sap.ui.getCore().byId("idcattle--poultryChart").removeStyleClass("Hide");
			sap.ui.getCore().byId("idcattle--cattleChart").addStyleClass("Hide");
			sap.ui.getCore().byId("idcattle--cattleChart").removeStyleClass("Show");
			sap.ui.getCore().byId("idcattle--exportsChart").addStyleClass("Hide");
			sap.ui.getCore().byId("idcattle--exportsChart").removeStyleClass("Show");
			sap.ui.getCore().byId("idcattle--combineChart").addStyleClass("Hide");
			sap.ui.getCore().byId("idcattle--combineChart").removeStyleClass("Show");
			}else if(Selectedvalue=="Cattle"){
				sap.ui.getCore().byId("idcattle--cattleChart").addStyleClass("Show");
				sap.ui.getCore().byId("idcattle--cattleChart").removeStyleClass("Hide");
				sap.ui.getCore().byId("idcattle--poultryChart").addStyleClass("Hide");
				sap.ui.getCore().byId("idcattle--poultryChart").removeStyleClass("Show");
				sap.ui.getCore().byId("idcattle--exportsChart").addStyleClass("Hide");
				sap.ui.getCore().byId("idcattle--exportsChart").removeStyleClass("Show");
				sap.ui.getCore().byId("idcattle--combineChart").addStyleClass("Hide");
				sap.ui.getCore().byId("idcattle--combineChart").removeStyleClass("Show");
				}else if(Selectedvalue=="Exports"){
					sap.ui.getCore().byId("idcattle--exportsChart").addStyleClass("Show");
					sap.ui.getCore().byId("idcattle--exportsChart").removeStyleClass("Hide");
					sap.ui.getCore().byId("idcattle--cattleChart").addStyleClass("Hide");
					sap.ui.getCore().byId("idcattle--cattleChart").removeStyleClass("Show");
					sap.ui.getCore().byId("idcattle--poultryChart").addStyleClass("Hide");
					sap.ui.getCore().byId("idcattle--poultryChart").removeStyleClass("Show");
					sap.ui.getCore().byId("idcattle--combineChart").addStyleClass("Hide");
					sap.ui.getCore().byId("idcattle--combineChart").removeStyleClass("Show");
					}else if(Selectedvalue=="All"){
						sap.ui.getCore().byId("idcattle--combineChart").addStyleClass("Show");
						sap.ui.getCore().byId("idcattle--combineChart").removeStyleClass("Hide");
						sap.ui.getCore().byId("idcattle--exportsChart").addStyleClass("Hide");
						sap.ui.getCore().byId("idcattle--exportsChart").removeStyleClass("Show");
						sap.ui.getCore().byId("idcattle--cattleChart").addStyleClass("Hide");
						sap.ui.getCore().byId("idcattle--cattleChart").removeStyleClass("Show");
						sap.ui.getCore().byId("idcattle--poultryChart").addStyleClass("Hide");
						sap.ui.getCore().byId("idcattle--poultryChart").removeStyleClass("Show");
						}
	  },

});