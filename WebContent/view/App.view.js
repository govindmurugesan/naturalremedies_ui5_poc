sap.ui.jsview("chart3D.view.App", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf view.App
	*/ 
	getControllerName : function() {
		return "chart3D.view.App";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf view.App
	*/ 
	createContent : function(oController) {
		var app = new sap.m.App("reference",{initialPage:"iddashboard"}); 
		var dashboard = sap.ui.view({id:"iddashboard", viewName:"chart3D.view.dashboard", type:sap.ui.core.mvc.ViewType.XML});
		var subDashboard = sap.ui.view({id:"idsubDashboard", viewName:"chart3D.view.subDashboard", type:sap.ui.core.mvc.ViewType.XML});
		var cattle = sap.ui.view({id:"idcattle", viewName:"chart3D.view.cattle", type:sap.ui.core.mvc.ViewType.XML});
		var monthWise = sap.ui.view({id:"idmonthWise", viewName:"chart3D.view.monthWise", type:sap.ui.core.mvc.ViewType.XML});
		var monthWiseTotal = sap.ui.view({id:"idmonthWiseTotal", viewName:"chart3D.view.monthWiseTotal", type:sap.ui.core.mvc.ViewType.XML});
		
		app.addPage(dashboard);
		app.addPage(subDashboard);
		app.addPage(cattle);
		app.addPage(monthWise);
		app.addPage(monthWiseTotal);
		
		app.placeAt("content");
	}

});