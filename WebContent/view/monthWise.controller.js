sap.ui.controller("chart3D.view.monthWise", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf view.monthWise
*/
//	onInit: function() {
//
//	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf view.monthWise
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf view.monthWise
*/
	onAfterRendering: function() {
		sap.ui.getCore().byId("idmonthWise--monthlyCattleChart").addStyleClass("Show");
		sap.ui.getCore().byId("idmonthWise--monthlyPoultryChart").addStyleClass("Hide");
		sap.ui.getCore().byId("idmonthWise--monthlyExportsChart").addStyleClass("Hide");
		sap.ui.getCore().byId("idmonthWise--monthlyAllChart").addStyleClass("Hide");
		
		oController = this;
		jQuery(function() {
			var chart = oController.getView().byId("monthlyCattleChart");
			chart = chart.getId();
			$("#" + chart).highcharts({
				chart: {
					type: "column",
					options3d: { enabled: true, alpha: 10, beta: 10, viewDistance: 15, depth: 50 }
				},
				xAxis: {
			        categories: ['Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
			    },
			    yAxis: {
			        allowDecimals: false,
			        title: {
			            text: '',
			            style: { color: "#4DA74D" }
			        }
			    },colors: ["#E7191B", "#FBB321", "#EF8424", "#C2C2C2", "#4DA74D"], 
				title: {
			        text: 'Cattle Sales',
			        style: { color: "#4DA74D" }
			    },
				plotOptions: {
			        column: { stacking: 'normal', depth: 20 }
			    },
			    credits: { enabled: false },
			    series: [
						{ name: '15-16',data: [7.48, 7.00, 7.74, 8.70, 9.55, 10.86, 11.08, 9.55, 9.91],"stack": "1"},
						{ name: '16-17', data: [8.04, 7.42, 8.46, 10.34, 11.40, 11.88, 10.29, 9.28, 7.68],"stack": "2"},
						{ name: 'GR', data: [8, 6, 9, 19, 19, 9.36, -7, -3, -22],"stack": "3" }
						/*{ name: 'Poultry 15-16',data: [3.43, 2.52, 3.11, 2.74, 2.87, 2.66, 2.65, 2.50, 2.69]},
						  { name: 'Poultry 16-17',data: [3.49, 2.93, 3.28, 3.00, 3.07, 2.51, 2.94, 2.62, 2.88]},
						  { name: 'Poultry GR',data: [2, 16, 6, 10, 7, -5.51, 11, 5, 7]},
						{ name: 'Exports 15-16',data: [0.96, 0.89, 2.26, 1.72, 1.92, 1.33, 1.29, 1.02, 1.78]},
						{ name: 'Exports 16-17',data: [1.19, 1.07, 2.06, 1.83, 1.38, 1.27, 0.80, 1.54, 1.70]},
						{ name: 'Exports GR',data: [24, 19, -9, 7, -28, -3.94, -38, 51, -4]},
						 	{ name: 'Q1 15-16',data: [22.22, ]},
						 	{ name: 'Q2 16-17',data: [23.93, ]},
						 	{ name: 'Q3 GR',data: [8, ]},*/
					] });
			});
//////////////////////// monthlyPoultryChart /////////////
		oController = this;
		jQuery(function() {
			var chart = oController.getView().byId("monthlyPoultryChart");
			chart = chart.getId();
			$("#" + chart).highcharts({
				chart: {
					type: "column",
					options3d: { enabled: true, alpha: 10, beta: 10, viewDistance: 15, depth: 50 }
				},
				xAxis: {
			        categories: ['Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
			    },
			    yAxis: {
			        allowDecimals: false,
			        title: {
			            text: '',
			            style: { color: "#4DA74D" }
			        }
			    },colors: ["#4DA74D", "#EF8424", "#FBB321", "#C2C2C2", "#E7191B"], 
				title: {
			        text: 'Poultry Sales',
			        style: { color: "#4DA74D" }
			    },
				plotOptions: {
			        column: { stacking: 'normal', depth: 20 }
			    },
			    credits: { enabled: false },
			    series: [
							{ name: 'Poultry 15-16',data: [3.43, 2.52, 3.11, 2.74, 2.87, 2.66, 2.65, 2.50, 2.69]},
							{ name: 'Poultry 16-17',data: [3.49, 2.93, 3.28, 3.00, 3.07, 2.51, 2.94, 2.62, 2.88]},
							{ name: 'Poultry GR',data: [2, 16, 6, 10, 7, -5.51, 11, 5, 7]}
						] });
			});
//////////////////////// monthlyExportsChart /////////////
		oController = this;
		jQuery(function() {
		var data = [
					{ "month":"Apr","name": 'Exports 15-16',"val": [0.96]},
					{ "month":"May","name": 'Exports 15-16',"val": [0.89]},
					{ "month":"Jun","name": 'Exports 15-16',"val": [2.26]},
					{ "month":"Jul","name": 'Exports 15-16',"val": [1.72]},
					{ "month":"Aug","name": 'Exports 15-16',"val": [1.92]},
					{ "month":"Sep","name": 'Exports 15-16',"val": [1.33]},
					{ "month":"Oct","name": 'Exports 15-16',"val": [1.29]},
					{ "month":"Nov","name": 'Exports 15-16',"val": [1.02]},
					{ "month":"Dec","name": 'Exports 15-16',"val": [1.78]},
						{ "month":"Apr","name": 'Exports 16-17',"val": [1.19]},
						{ "month":"May","name": 'Exports 16-17',"val": [1.07]},
						{ "month":"Jun","name": 'Exports 16-17',"val": [2.06]},
						{ "month":"Jul","name": 'Exports 16-17',"val": [1.83]},
						{ "month":"Aug","name": 'Exports 16-17',"val": [1.38]},
						{ "month":"Sep","name": 'Exports 16-17',"val": [1.27]},
						{ "month":"Oct","name": 'Exports 16-17',"val": [0.80]},
						{ "month":"Nov","name": 'Exports 16-17',"val": [1.54]},
						{ "month":"Dec","name": 'Exports 16-17',"val": [1.70]},
					{ "month":"Apr","name": 'Exports GR',"val": [24]},
					{ "month":"May","name": 'Exports GR',"val": [19]},
					{ "month":"Jun","name": 'Exports GR',"val": [-9]},
					{ "month":"Jul","name": 'Exports GR',"val": [7]},
					{ "month":"Aug","name": 'Exports GR',"val": [-28]},
					{ "month":"Sep","name": 'Exports GR',"val": [-3.94]},
					{ "month":"Oct","name": 'Exports GR',"val": [-38]},
					{ "month":"Nov","name": 'Exports GR',"val": [51]},
					{ "month":"Dec","name": 'Exports GR',"val": [-4]}
			];
			    
		var seriesData = [];
		var xCategories = [];
		var i;
		for(i = 0; i < data.length; i++){
		    if(seriesData){
		      var currSeries = seriesData.filter(function(seriesObject){ return seriesObject.name == data[i].name;});
		      if(currSeries.length === 0){
		          seriesData[seriesData.length] = currSeries = {name: data[i].name, data: []};
		      } else {
		          currSeries = currSeries[0];
		      }
		      var index = currSeries.data.length;
		      currSeries.data[index] = data[i].val;
		    } else {
		       seriesData[0] = {name: data[i].name, data: [data[i].val]}
		    }
		}
			
			var chart = oController.getView().byId("monthlyExportsChart");
			chart = chart.getId();
			$("#" + chart).highcharts({
				chart: {
					type: "column",
					options3d: { enabled: true, alpha: 10, beta: 10, viewDistance: 15, depth: 50 }
				},
				xAxis: {
			        categories: ['Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
				},
			    yAxis: {
			        allowDecimals: false,
			        title: {
			            text: '',
			            style: { color: "#4DA74D" }
			        }
			    },colors: ["#EF8424", "#C2C2C2", "#FBB321"], 
				title: {
			        text: 'Exports Sales',
			        style: { color: "#4DA74D" }
			    },
				plotOptions: {
			        column: { stacking: 'normal', depth: 20 }
			    },
			    credits: { enabled: false },
			    series: seriesData
			    /*series: [
						{ name: 'Exports 15-16',data: [0.96, 0.89, 2.26, 1.72, 1.92, 1.33, 1.29, 1.02, 1.78]},
						{ name: 'Exports 16-17',data: [1.19, 1.07, 2.06, 1.83, 1.38, 1.27, 0.80, 1.54, 1.70]},
						{ name: 'Exports GR',data: [24, 19, -9, 7, -28, -3.94, -38, 51, -4]}
					] */
			    });
			});
//////////////////////  monthlyAllChart   ////////////////
		jQuery(function() {
			var chart = oController.getView().byId("monthlyAllChart");
			chart = chart.getId();
			$("#" + chart).highcharts({
				chart: {
					type: "column",
					options3d: { enabled: true, alpha: 10, beta: 10, viewDistance: 15, depth: 50 }
				},
				xAxis: {
			        categories: ['Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
			    },
			    yAxis: {
			        allowDecimals: false,
			        title: {
			            text: '',
			            style: { color: "#4DA74D" }
			        }
			    },colors: ["#E7191B", "#FBB321", "#EF8424", "#C2C2C2", "#4DA74D"], 
				title: {
			        text: 'All Divisions',
			        style: { color: "#4DA74D" }
			    },
				plotOptions: {
			        column: { stacking: 'normal', depth: 20 }
			    },
			    credits: { enabled: false },
			    series: [
						{ name: 'Cattle 15-16',data: [7.48, 7.00, 7.74, 8.70, 9.55, 10.86, 11.08, 9.55, 9.91],"stack": "1"},
						{ name: 'Cattle 16-17', data: [8.04, 7.42, 8.46, 10.34, 11.40, 11.88, 10.29, 9.28, 7.68],"stack": "2"},
						{ name: 'Cattle GR', data: [8, 6, 9, 19, 19, 9.36, -7, -3, -22],"stack": "3" },
						{ name: 'Poultry 15-16',data: [3.43, 2.52, 3.11, 2.74, 2.87, 2.66, 2.65, 2.50, 2.69],"stack": "1"},
						{ name: 'Poultry 16-17',data: [3.49, 2.93, 3.28, 3.00, 3.07, 2.51, 2.94, 2.62, 2.88],"stack": "2"},
						{ name: 'Poultry GR',data: [2, 16, 6, 10, 7, -5.51, 11, 5, 7],"stack": "3"},
						{ name: 'Exports 15-16',data: [0.96, 0.89, 2.26, 1.72, 1.92, 1.33, 1.29, 1.02, 1.78],"stack": "1"},
						{ name: 'Exports 16-17',data: [1.19, 1.07, 2.06, 1.83, 1.38, 1.27, 0.80, 1.54, 1.70],"stack": "2"},
						{ name: 'Exports GR',data: [24, 19, -9, 7, -28, -3.94, -38, 51, -4],"stack": "3"}
					] });
			});
		
	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf view.monthWise
*/
//	onExit: function() {
//
//	}
	onBack: function(){
		sap.ui.getCore().byId("reference").to("idsubDashboard");
	  },
	  selectOption: function(){
			var Selectedvalue =	this.getView().byId("comboId").getSelectedItem().getText();
			if(Selectedvalue=="Cattle"){
				sap.ui.getCore().byId("idmonthWise--monthlyCattleChart").addStyleClass("Show");
				sap.ui.getCore().byId("idmonthWise--monthlyCattleChart").removeStyleClass("Hide");
				sap.ui.getCore().byId("idmonthWise--monthlyPoultryChart").addStyleClass("Hide");
				sap.ui.getCore().byId("idmonthWise--monthlyPoultryChart").removeStyleClass("Show");
				sap.ui.getCore().byId("idmonthWise--monthlyExportsChart").addStyleClass("Hide");
				sap.ui.getCore().byId("idmonthWise--monthlyExportsChart").removeStyleClass("Show");
				sap.ui.getCore().byId("idmonthWise--monthlyAllChart").addStyleClass("Hide");
				sap.ui.getCore().byId("idmonthWise--monthlyAllChart").removeStyleClass("Show");
				}else if(Selectedvalue=="Poultry"){
					sap.ui.getCore().byId("idmonthWise--monthlyPoultryChart").addStyleClass("Show");
					sap.ui.getCore().byId("idmonthWise--monthlyPoultryChart").removeStyleClass("Hide");
					sap.ui.getCore().byId("idmonthWise--monthlyCattleChart").addStyleClass("Hide");
					sap.ui.getCore().byId("idmonthWise--monthlyCattleChart").removeStyleClass("Show");
					sap.ui.getCore().byId("idmonthWise--monthlyExportsChart").addStyleClass("Hide");
					sap.ui.getCore().byId("idmonthWise--monthlyExportsChart").removeStyleClass("Show");
					sap.ui.getCore().byId("idmonthWise--monthlyAllChart").addStyleClass("Hide");
					sap.ui.getCore().byId("idmonthWise--monthlyAllChart").removeStyleClass("Show");
					}else if(Selectedvalue=="Exports"){
						sap.ui.getCore().byId("idmonthWise--monthlyExportsChart").addStyleClass("Show");
						sap.ui.getCore().byId("idmonthWise--monthlyExportsChart").removeStyleClass("Hide");
						sap.ui.getCore().byId("idmonthWise--monthlyPoultryChart").addStyleClass("Hide");
						sap.ui.getCore().byId("idmonthWise--monthlyPoultryChart").removeStyleClass("Show");
						sap.ui.getCore().byId("idmonthWise--monthlyCattleChart").addStyleClass("Hide");
						sap.ui.getCore().byId("idmonthWise--monthlyCattleChart").removeStyleClass("Show");
						sap.ui.getCore().byId("idmonthWise--monthlyAllChart").addStyleClass("Hide");
						sap.ui.getCore().byId("idmonthWise--monthlyAllChart").removeStyleClass("Show");
						}else if(Selectedvalue=="All"){
							sap.ui.getCore().byId("idmonthWise--monthlyAllChart").addStyleClass("Show");
							sap.ui.getCore().byId("idmonthWise--monthlyAllChart").removeStyleClass("Hide");
							sap.ui.getCore().byId("idmonthWise--monthlyExportsChart").addStyleClass("Hide");
							sap.ui.getCore().byId("idmonthWise--monthlyExportsChart").removeStyleClass("Show");
							sap.ui.getCore().byId("idmonthWise--monthlyPoultryChart").addStyleClass("Hide");
							sap.ui.getCore().byId("idmonthWise--monthlyPoultryChart").removeStyleClass("Show");
							sap.ui.getCore().byId("idmonthWise--monthlyCattleChart").addStyleClass("Hide");
							sap.ui.getCore().byId("idmonthWise--monthlyCattleChart").removeStyleClass("Show");
							}
	  },
});