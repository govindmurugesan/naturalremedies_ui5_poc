sap.ui.controller("chart3D.view.monthWiseTotal", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf view.monthWiseTotal
*/
//	onInit: function() {
//
//	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf view.monthWiseTotal
*/
//	onBeforeRendering: function() {
//
//	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf view.monthWiseTotal
*/
	onAfterRendering: function() {
		sap.ui.getCore().byId("idmonthWiseTotal--Q1Chart").addStyleClass("Show");
		sap.ui.getCore().byId("idmonthWiseTotal--Q2Chart").addStyleClass("Hide");
		sap.ui.getCore().byId("idmonthWiseTotal--Q3Chart").addStyleClass("Hide");
		sap.ui.getCore().byId("idmonthWiseTotal--Total").addStyleClass("Hide");
		
		oController = this;
		jQuery(function() {
			var chart = oController.getView().byId("Q1Chart");
			
			chart = chart.getId();
			$("#" + chart).highcharts({
				chart: {
					type: "column",
					options3d: { enabled: true, alpha: 10, beta: 10, viewDistance: 15, depth: 50 }
				},
				xAxis: {
			        categories: ['Cattle', 'Poultry', 'Exports']
			    },
			    yAxis: {
			        allowDecimals: false,
			        title: {
			            text: '',
			           style: { color: "#4DA74D" }
			        }
			    },
				 colors: ["#FBB321", "#4DA74D", "#E7191B", "#EF8424", "#C2C2C2"], 
				title: {
			        text: 'First Quarter',
			        style: { color: "#4DA74D" }
			    },
				plotOptions: {
			        column: {
			            stacking: 'normal',
			            depth: 20
			        }
			    },
			    credits: {
			        enabled: false
			    },
				series: [
						{ "name": '15-16',"data": [22.22, 9.05, 4.12],"stack": "1"},
						{ "name": '16-17', "data": [23.93, 9.71, 4.32],"stack": "2"},
						{ "name": 'GR', "data": [8, 7, 5],"stack": "3" }
				        ]
			});
//////////////////////  Q2Chart /////////////
			var chart = oController.getView().byId("Q2Chart");
			chart = chart.getId();
			$("#" + chart).highcharts({
				chart: {
					type: "column",
					options3d: { enabled: true, alpha: 10, beta: 10, viewDistance: 15, depth: 50 }
				},
				xAxis: {
			        categories: ['Cattle', 'Poultry', 'Exports']
			    },
			    yAxis: {
			        allowDecimals: false,
			        title: {
			            text: '',
			           style: { color: "#4DA74D" }
			        }
			    },
				 colors: ["#E7191B", "#EF8424", "#FBB321", "#4DA74D", "#C2C2C2"], 
				title: {
			        text: 'Second Quarter',
			        style: { color: "#4DA74D" }
			    },
				plotOptions: {
			        column: {
			            stacking: 'normal',
			            depth: 20
			        }
			    },
			    credits: {
			        enabled: false
			    },
				series: [
						{ "name": '15-16',"data": [29.11, 8.26, 4.97],"stack": "1"},
						{ "name": '16-17', "data": [33.62, 8.57, 4.49],"stack": "2"},
						{ "name": 'GR', "data": [15, 4, -10],"stack": "3" }
				     ]});
////////////////////// Q3Chart /////////////
			var chart = oController.getView().byId("Q3Chart");
			chart = chart.getId();
			$("#" + chart).highcharts({
				chart: {
					type: "column",
					options3d: { enabled: true, alpha: 10, beta: 10, viewDistance: 15, depth: 50 }
				},
				xAxis: {
			        categories: ['Cattle', 'Poultry', 'Exports']
			    },
			    yAxis: {
			        allowDecimals: false,
			        title: {
			            text: '',
			           style: { color: "#4DA74D" }
			        }
			    },
				 colors: ["#E7191B", "#EF8424", "#FBB321", "#4DA74D", "#C2C2C2"], 
				title: {
			        text: 'Third Quarter',
			        style: { color: "#4DA74D" }
			    },
				plotOptions: {
			        column: {
			            stacking: 'normal',
			            depth: 20
			        }
			    },
			    credits: {
			        enabled: false
			    },
				series: [
						{ "name": '15-16',"data": [30.54, 7.48, 4.09],"stack": "1"},
						{ "name": '16-17', "data": [27.25, 8.44, 4.05],"stack": "2"},
						{ "name": 'GR', "data": [-11, 8, -1],"stack": "3" }
				     ]});
////////////////////// Total sChart /////////////
			var chart = oController.getView().byId("Total");
			chart = chart.getId();
			$("#" + chart).highcharts({
				chart: {
					type: "column",
					options3d: { enabled: true, alpha: 10, beta: 10, viewDistance: 15, depth: 50 }
				},
				xAxis: {
			        categories: ['Cattle', 'Poultry', 'Exports','Grand Total']
			    },
			    yAxis: {
			        allowDecimals: false,
			        title: {
			            text: '',
			           style: { color: "#4DA74D" }
			        }
			    },
				 colors: ["#E7191B", "#EF8424", "#FBB321", "#4DA74D", "#C2C2C2"], 
				title: {
			        text: 'Quarterly Total',
			        style: { color: "#4DA74D" }
			    },
				plotOptions: {
			        column: {
			            stacking: 'normal',
			            depth: 20
			        }
			    },
			    credits: {
			        enabled: false
			    },
				series: [
					{ "name": '15-16',"data": [81.87, 25.15, 13.17],"stack": "1"},
					{ "name": '16-17', "data": [84.80, 26.73, 12.86],"stack": "2"},
					{ "name": 'GR', "data": [4, 6, -2],"stack": "3" }
					/*{ "name": 'Grand Total', "data": [124.22, 128.70, 4],"stack": "4" }*/
						/*{ "name": 'Q1 15-16',"data": [22.22, 9.05, 4.12],"stack": "1"},
						{ "name": 'Q1 16-17', "data": [23.93, 9.71, 4.32],"stack": "2"},
						{ "name": 'Q1 GR', "data": [8, 7, 5],"stack": "3" },
						{ "name": 'Q2 15-16',"data": [29.11, 8.26, 4.97],"stack": "1"},
						{ "name": 'Q2 16-17', "data": [33.62, 8.57, 4.49],"stack": "2"},
						{ "name": 'Q2 GR', "data": [15, 4, -10],"stack": "3" },
						{ "name": 'Q3 15-16',"data": [30.54, 7.48, 4.09],"stack": "1"},
						{ "name": 'Q3 16-17', "data": [27.25, 8.44, 4.05],"stack": "2"},
						{ "name": 'Q3 GR', "data": [-11, 8, -1],"stack": "3" },
						{ "name": 'Total 15-16',"data": [81.87, 25.15, 13.17],"stack": "1"},
						{ "name": 'Total 16-17', "data": [84.80, 26.73, 12.86],"stack": "2"},
						{ "name": 'Total GR', "data": [4, 6, -2],"stack": "3" }*/
				     ]});
			
		});
		
	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf view.monthWiseTotal
*/
//	onExit: function() {
//
//	}
	onBack: function(){
		sap.ui.getCore().byId("reference").to("idsubDashboard");
	  },
	  selectOption: function(){
			var Selectedvalue =	this.getView().byId("comboId").getSelectedItem().getText();
			if(Selectedvalue=="Q1"){
				sap.ui.getCore().byId("idmonthWiseTotal--Q1Chart").addStyleClass("Show");
				sap.ui.getCore().byId("idmonthWiseTotal--Q1Chart").removeStyleClass("Hide");
				sap.ui.getCore().byId("idmonthWiseTotal--Q2Chart").addStyleClass("Hide");
				sap.ui.getCore().byId("idmonthWiseTotal--Q2Chart").removeStyleClass("Show");
				sap.ui.getCore().byId("idmonthWiseTotal--Q3Chart").addStyleClass("Hide");
				sap.ui.getCore().byId("idmonthWiseTotal--Q3Chart").removeStyleClass("Show");
				sap.ui.getCore().byId("idmonthWiseTotal--Total").addStyleClass("Hide");
				sap.ui.getCore().byId("idmonthWiseTotal--Total").removeStyleClass("Show");
				}else if(Selectedvalue=="Q2"){
					sap.ui.getCore().byId("idmonthWiseTotal--Q2Chart").addStyleClass("Show");
					sap.ui.getCore().byId("idmonthWiseTotal--Q2Chart").removeStyleClass("Hide");
					sap.ui.getCore().byId("idmonthWiseTotal--Q1Chart").addStyleClass("Hide");
					sap.ui.getCore().byId("idmonthWiseTotal--Q1Chart").removeStyleClass("Show");
					sap.ui.getCore().byId("idmonthWiseTotal--Q3Chart").addStyleClass("Hide");
					sap.ui.getCore().byId("idmonthWiseTotal--Q3Chart").removeStyleClass("Show");
					sap.ui.getCore().byId("idmonthWiseTotal--Total").addStyleClass("Hide");
					sap.ui.getCore().byId("idmonthWiseTotal--Total").removeStyleClass("Show");
					}else if(Selectedvalue=="Q3"){
						sap.ui.getCore().byId("idmonthWiseTotal--Q3Chart").addStyleClass("Show");
						sap.ui.getCore().byId("idmonthWiseTotal--Q3Chart").removeStyleClass("Hide");
						sap.ui.getCore().byId("idmonthWiseTotal--Q2Chart").addStyleClass("Hide");
						sap.ui.getCore().byId("idmonthWiseTotal--Q2Chart").removeStyleClass("Show");
						sap.ui.getCore().byId("idmonthWiseTotal--Q1Chart").addStyleClass("Hide");
						sap.ui.getCore().byId("idmonthWiseTotal--Q1Chart").removeStyleClass("Show");
						sap.ui.getCore().byId("idmonthWiseTotal--Total").addStyleClass("Hide");
						sap.ui.getCore().byId("idmonthWiseTotal--Total").removeStyleClass("Show");
						}else if(Selectedvalue=="Total"){
							sap.ui.getCore().byId("idmonthWiseTotal--Total").addStyleClass("Show");
							sap.ui.getCore().byId("idmonthWiseTotal--Total").removeStyleClass("Hide");
							sap.ui.getCore().byId("idmonthWiseTotal--Q3Chart").addStyleClass("Hide");
							sap.ui.getCore().byId("idmonthWiseTotal--Q3Chart").removeStyleClass("Show");
							sap.ui.getCore().byId("idmonthWiseTotal--Q2Chart").addStyleClass("Hide");
							sap.ui.getCore().byId("idmonthWiseTotal--Q2Chart").removeStyleClass("Show");
							sap.ui.getCore().byId("idmonthWiseTotal--Q1Chart").addStyleClass("Hide");
							sap.ui.getCore().byId("idmonthWiseTotal--Q1Chart").removeStyleClass("Show");
							}
	  },
});